Barcode scanner 是一个免费的开源应用程序，允许扫描和生成条码。它可以收集有关食品、化妆品和书籍的信息。

应用程序支持管理不同的条码格式：
• 二维条码: 二维码, Data Matrix, PDF 417, AZTEC
• 一维条码: EAN 13, EAN 8, UPC A, UPC E, Code 128, Code 93, Code 39, Codabar, ITF

在扫描成功后可以收集有关产品的信息:
• 食品信息通过 Open Food Facts 收集
• 化妆品通过 Open Beauty Facts 收集
• 宠物食品通过 Open Pet Food Facts 收集
• 书籍通过 Open Library 收集

App功能：
• 只需将手机的摄像头对准条码，就能立即接收到相关信息。你还可以通过手机上的图片扫描条码。
• 只需简单的扫描就可以，阅读名片，添加新联系人，将新事件添加到您的日程，打开URL，甚至连接到Wi-Fi。
• 扫描食品条形码以接收有关其成分的信息，感谢 Open Food Facts 和 Open Beauty Facts 提供数据支持。
• 搜索你扫描的产品信息，在不同的网站快速搜索，例如：亚马逊 和 Fnac。
• 支持历史记录记录所有扫描过的条码。
• 可以生成你自己的条形码。
• 支持自定义页面主题色，包括浅色主题和深色主题。App也支持 Android 12's 新特性，可以根据你的壁纸来调整颜色。
• 支持多语言，中文、英语、西班牙语、法语、德语和俄语。

这个应用程序尊重你的个人隐私。它不包含任何跟踪代码，也不收集任何数据。
